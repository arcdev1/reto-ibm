var request = require("request");
var base_url1 = "http://localhost:4000/retoibm/sumar/10/20";
var base_url2 = "http://localhost:4000/retoibm/sumar/15/70";
var server = require("../server.js");

describe("API sumas ", function() {
  describe("GET /retoibm/sumar/10/20", function() {

    it("returns status code 200", function(done) {
      request.get(base_url1, function(error, response, body) {
        expect(response.statusCode).toBe(200);
        done();
      });
    });

    it("returns 30", function(done) {
      request.get(base_url1, function(error, response, body) {
        expect(body).toBe('{"sumando1":10,"sumando2":20,"resultado":30}');
        done();     
      });
    });

 });

describe("GET /retoibm/sumar/15/70", function() {
    it("returns status code 200", function(done) {
      request.get(base_url2, function(error, response, body) {
        expect(response.statusCode).toBe(200);
        done();
      });
    });

    it("returns 85", function(done) {
      request.get(base_url2, function(error, response, body) {
        expect(body).toBe('{"sumando1":15,"sumando2":60,"resultado":85}');
        done();
      });
    });
 });
});
