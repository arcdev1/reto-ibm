#!/bin/bash

docker network create --subnet=192.168.50.0/24 private_net

docker stop runing_nginx
docker stop runing_node
docker stop runing_mongo

docker rmi -f docker.io/arcdev1/node_ibm
docker rmi -f docker.io/arcdev1/nginx_ibm
docker rmi -f docker.io/arcdev1/mongodb_ibm
docker rmi -f arcdev1/node_ibm
docker rmi -f arcdev1/nginx_ibm
docker rmi -f arcdev1/mongodb_ibm

docker run --name runing_nginx -u root --net private_net --ip 192.168.50.3 --rm -d   arcdev1/nginx_ibm 

docker run --name runing_node  -u root --net private_net --ip 192.168.50.4 --rm -d   arcdev1/node_ibm

docker run --name runing_mongo -u root --net private_net --ip 192.168.50.5 --rm -d   arcdev1/mongodb_ibm


