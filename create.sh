#!/bin/bash
docker build -f ./nginx/Dockerfile -t arcdev1/nginx_ibm ./nginx/
docker build -f ./nodejs/Dockerfile -t arcdev1/node_ibm ./nodejs/
docker build -f ./mongodb/Dockerfile -t arcdev1/mongodb_ibm ./mongodb/
